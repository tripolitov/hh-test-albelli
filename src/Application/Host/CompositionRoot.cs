﻿using System;
using Application.Windsor;
using Castle.Facilities.Logging;
using Castle.Facilities.Startable;
using Castle.Facilities.TypedFactory;
using Castle.Services.Logging.SerilogIntegration;
using Castle.Windsor;

namespace Application.Host
{
    internal static class CompositionRoot
    {
        public static IDisposable Compose(Configuration configuration)
        {
            var container = new WindsorContainer();
            container.AddFacility<LoggingFacility>(f => f.LogUsing<SerilogFactory>());
            container.AddFacility<TypedFactoryFacility>();
            container.AddFacility<StartableFacility>(f => f.DeferredStart());
            container.Install(new ApplicationInstaller(configuration));
            return container;

        }
    }
}
