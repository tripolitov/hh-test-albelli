﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Application.Host
{
    partial class ServiceHost : ServiceBase
    {
        private readonly Func<IDisposable> m_Bootstrapper;
        private IDisposable m_Host;

        public ServiceHost(Func<IDisposable> bootstrapper)
        {
            if (bootstrapper == null) throw new ArgumentNullException(nameof(bootstrapper));
            m_Bootstrapper = bootstrapper;
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            m_Host = m_Bootstrapper();
        }

        protected override void OnStop()
        {
            m_Host.Dispose();
        }
    }
}
