﻿using System;
using System.Configuration;

namespace Application.Host
{
    internal class Configuration
    {
        public string ConnectionString { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }

        public Uri BaseUrl {
            get { return new Uri(string.Format("http://{0}:{1}", Host, Port)); }
        }

        public static Configuration FromAppConfig()
        {
            //TODO[MT]: replace with configuration section
            var result = new Configuration()
            {
                Host = "localhost",
                Port = 8080
            };
            var connectionStringSettings = ConfigurationManager.ConnectionStrings["main"];
            if (connectionStringSettings == null)
            {
                throw new ConfigurationErrorsException("Configure connection string 'main' via app.config.");
            }
            result.ConnectionString = connectionStringSettings.ConnectionString;
            if (ConfigurationManager.AppSettings["host"] != null)
            {
                result.Host = ConfigurationManager.AppSettings["host"];
            }
            int port;
            if (ConfigurationManager.AppSettings["port"] != null && int.TryParse(ConfigurationManager.AppSettings["port"], out port))
            {
                result.Port = port;
            }
            return result;
        }
    }
}