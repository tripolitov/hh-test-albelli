﻿using System;
using System.IO;
using System.ServiceProcess;

namespace Application.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!Environment.UserInteractive)
            {
                Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
                var servicesToRun = new ServiceBase[] { new ServiceHost(createApplication) };
                ServiceBase.Run(servicesToRun);
                return;
            }

            using (createApplication())
            {
                Console.ReadLine();
            }
        }

        static IDisposable createApplication()
        {
            return CompositionRoot.Compose(Configuration.FromAppConfig());
        }
    }

    
}
