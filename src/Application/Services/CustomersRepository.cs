﻿using System;
using System.Data.Entity;
using System.Linq;
using Application.Data;
using Application.Domain;

namespace Application.Services
{
    class CustomersRepository : ICustomersRepository
    {
        private readonly DbContext m_DbContext;
        private readonly IDbSet<Customer> m_DbSet;

        public CustomersRepository(OrdersContext dbContext)
        {
            m_DbContext = dbContext;
            m_DbSet = dbContext.Customers;
        }

        public IQueryable<Customer> GetAll()
        {
            return m_DbSet;
        }

        public Customer Get(int id)
        {
            return m_DbSet.Include(c => c.Orders).FirstOrDefault(o => o.Id == id);
        }

        public Customer Add(Customer customer)
        {
            if (customer == null) throw new ArgumentNullException(nameof(customer));

            var dbEntry = m_DbContext.Entry(customer);
            if (dbEntry.State == EntityState.Detached)
            {
                dbEntry.State = EntityState.Added;
            }
            else
            {
                m_DbSet.Add(customer);
            }

            m_DbContext.SaveChanges();
            return dbEntry.Entity;
        }

        public Customer Update(Customer customer)
        {
            if (customer == null) throw new ArgumentNullException(nameof(customer));

            var dbEntry = m_DbContext.Entry(customer);
            if (dbEntry.State == EntityState.Detached)
            {
                m_DbSet.Attach(customer);
            }
            dbEntry.State = EntityState.Modified;

            m_DbContext.SaveChanges();
            return dbEntry.Entity;
        }

        public void Delete(Customer customer)
        {
            if (customer == null) throw new ArgumentNullException(nameof(customer));

            var dbEntry = m_DbContext.Entry(customer);
            if (dbEntry.State != EntityState.Deleted)
            {
                dbEntry.State = EntityState.Deleted;
            }
            else
            {
                m_DbSet.Attach(customer);
                m_DbSet.Remove(customer);
            }

            m_DbContext.SaveChanges();
        }
    }
}