﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Application.Domain;
using Application.WebApi.Model;
using Castle.Core.Internal;

namespace Application.Services
{
    public interface ICustomersRepository  
    {
        IQueryable<Customer> GetAll();
        Customer Get(int id);
        Customer Add(Customer customer);
        Customer Update(Customer customer);
        void Delete(Customer customer);
    }
}