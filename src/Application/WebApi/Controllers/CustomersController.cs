﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Application.Domain;
using Application.Services;
using Application.WebApi.Model;
using Castle.Core.Logging;

namespace Application.WebApi.Controllers
{
    /// <summary>
    /// Customers
    /// </summary>
    public class CustomersController : ApiController
    {
        private readonly ICustomersRepository m_CustomersRepository;

        public CustomersController(ICustomersRepository customersRepository)
        {
            if (customersRepository == null) throw new ArgumentNullException(nameof(customersRepository));
            m_CustomersRepository = customersRepository;
        }

        /// <summary>
        /// Customers
        /// </summary>
        /// <remarks>
        /// Gets a list of customers
        /// </remarks>
        /// <returns></returns>
        [HttpGet, ResponseType(typeof(CustomerListModel[]))]
        public IHttpActionResult GetAll()
        {
            var customers = m_CustomersRepository.GetAll().Select(c => new CustomerListModel() {Id = c.Id, Name = c.Name, Email = c.Email});
            return Ok(customers);
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id">Customer id</param>
        /// <returns>Customer data with list of orders</returns>
        /// <remarks>Get customer by it's identifier</remarks>
        [HttpGet, ResponseType(typeof(CustomerModel))]
        public IHttpActionResult Get(int id)
        {
            if (id <= 0) return BadRequest("Id should be greater then 0.");

            var customer = m_CustomersRepository.Get(id);
            if (customer == null) return NotFound();

            return Ok(new CustomerModel(customer));
        }

        /// <summary>
        /// Creates new client
        /// </summary>
        /// <param name="customer">Customer data</param>
        /// <returns>Customer data with list of orders</returns>
        /// <remarks>Create customer</remarks>
        [HttpPost, ResponseType(typeof(CustomerModel))]
        public IHttpActionResult Create(CustomerCreateModel customer)
        {
            if(customer == null) return BadRequest("Customer data not provided");

            var customerModel = m_CustomersRepository.Add(new Customer() { Name = customer.Name, Email = customer.Email});

            return Created(new Uri(Url.Link("RESTGet", new { id = customerModel.Id })), customerModel);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id">Customer id</param>
        /// <param name="customerData">Customer data</param>
        /// <returns>Customer data with list of orders</returns>
        /// <remarks>Update customer</remarks>
        [HttpPatch, ResponseType(typeof(CustomerModel))]
        public IHttpActionResult Update([FromUri]int id, [FromBody]CustomerUpdateModel customerData)
        {
            if (id <= 0) return BadRequest("Id should be greater then 0.");
            if (customerData == null) return BadRequest("Cusomer data not provided");

            var customer = m_CustomersRepository.Get(id);
            if (customer == null)
                return NotFound();

            customer.Name = customerData.Name;
            customer.Email = customerData.Email;

            var customerModel = m_CustomersRepository.Update(customer);
            
            return Ok(customerModel);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id">Customer id</param>
        /// <remarks>Delete customer by id</remarks>
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0) return BadRequest("Id should be greater then 0.");
            var customer = m_CustomersRepository.Get(id);
            if (customer == null) return NotFound();

            m_CustomersRepository.Delete(customer);

            return Ok();
        }

        /// <summary>
        /// Add order
        /// </summary>
        /// <param name="id">Customer id</param>
        /// <param name="orderData">Order data</param>
        /// <returns>Order added to customer</returns>
        /// <remarks>Add order to customer</remarks>
        [HttpPost, ResponseType(typeof(CustomerModel))]
        public IHttpActionResult AddOrder([FromUri]int id, [FromBody]OrderModel orderData)
        {
            if (id <= 0) return BadRequest("Id should be greater then 0.");
            if (orderData == null) return BadRequest("Order data not provided");
            //TODO[MT]: replace with fluent validatorsB
            if (!orderData.CreateDate.HasValue) return BadRequest("Order date not provided");
            if (!orderData.Price.HasValue) return BadRequest("Order date not provided");

            var customer = m_CustomersRepository.Get(id);
            if (customer == null) return NotFound();

            var order = new Order() {CreateDate = orderData.CreateDate.Value, Price = orderData.Price.Value};
            customer.Orders.Add(order);

            m_CustomersRepository.Update(customer);

            return Created(new Uri(Url.Link("RESTGet", new { id = id })), new OrderModel(order));
        }
    }
}