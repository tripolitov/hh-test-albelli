﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Castle.Core.Logging;

namespace Application.WebApi.MessageHandlers
{
    internal class LoggingHandler : DelegatingHandler
    {
        private readonly ILogger m_Logger;
        public LoggingHandler(ILogger logger)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            m_Logger = logger;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var trackingId = Guid.NewGuid();
            if (m_Logger.IsDebugEnabled)
            {
                string requestContent = "EMPTY";
                if (request.Content != null)
                {
                    var contentLength = request.Content.Headers.ContentLength ?? 0;
                    if (contentLength > 0)
                    {
                        requestContent = await request.Content.ReadAsStringAsync();
                    }
                }
                m_Logger.DebugFormat("Request [{1}] Request Uri: {2}{0}Request Headers:{0}{3}{0}Request Content:{0}{4}", Environment.NewLine, trackingId, request.RequestUri.ToString(), request.ToString(), requestContent);
            }

            try
            {
                var response = await base.SendAsync(request, cancellationToken);
                if (m_Logger.IsDebugEnabled)
                {
                    var responseContent = "EMPTY";
                    if (response != null)
                    {
                        var httpContent = response.Content;
                        if (httpContent != null)
                        {
                            if (httpContent.Headers != null && (httpContent.Headers.ContentLength == null || httpContent.Headers.ContentLength > 0))
                            {
                                responseContent = await httpContent.ReadAsStringAsync();
                            }
                        }
                    }
                    m_Logger.DebugFormat("Request [{1}] Request Uri: {2}{0}Response Headers:{0}{3}{0}Response Content:{0}{4}", Environment.NewLine, trackingId, request.RequestUri.ToString(), Convert.ToString(response), responseContent);
                }
                return response;
            }
            catch (Exception e)
            {
                m_Logger.ErrorFormat(e, "Request [{0}] produced error with message '{1}'", trackingId, e.Message);
                throw;
            }
        }
    }
}
