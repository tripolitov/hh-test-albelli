﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.Routing;
using Application.WebApi.MessageHandlers;
using Castle.Core;
using Castle.Core.Logging;
using Microsoft.Owin.Hosting;
using Owin;

namespace Application.WebApi
{
    class ApiHost : IDisposable, IStartable
    {
        private const int VERSION = 1;
        private readonly ILogger m_Logger;
        private readonly IHttpControllerActivator m_HttpControllerActivator;
        private readonly LoggingHandler m_LoggingHandler;
        private readonly Uri m_BaseUrl;

        public ApiHost(ILogger logger, 
                       IHttpControllerActivator httpControllerActivator,
                       LoggingHandler loggingHandler, 
                       Uri baseUrl)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (httpControllerActivator == null) throw new ArgumentNullException(nameof(httpControllerActivator));
            if (loggingHandler == null) throw new ArgumentNullException(nameof(loggingHandler));
            m_Logger = logger;
            m_HttpControllerActivator = httpControllerActivator;
            m_LoggingHandler = loggingHandler;
            m_BaseUrl = baseUrl;
        }

        private IDisposable m_WebApp = null;

        public void Start()
        {
            m_WebApp = WebApp.Start(m_BaseUrl.ToString(), builder => builder.UseWebApi(configure(new HttpConfiguration())));

            m_Logger.InfoFormat("Web api started on '{url}'", m_BaseUrl);
        }

        public void Stop()
        {
            if (m_WebApp != null)
            { 
                m_WebApp.Dispose();
                m_WebApp = null;
                m_Logger.InfoFormat("Web api stopped");
            }
        }

        HttpConfiguration configure(HttpConfiguration config)
        {
            config.Formatters.XmlFormatter.UseXmlSerializer = true;

            config.MessageHandlers.Insert(0, m_LoggingHandler);

            config.Services.Replace(typeof(IHttpControllerActivator), m_HttpControllerActivator);

            config.Routes.MapHttpRoute("RESTGetAll", string.Format("v{0}/customers", VERSION),
                new {action = "GetAll", controller = "Customers"},
                new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)});

            config.Routes.MapHttpRoute("RESTCreate", string.Format("v{0}/customers", VERSION),
               new { action = "Create", controller = "Customers" },
               new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            config.Routes.MapHttpRoute("RESTGet", string.Format("v{0}/customers/{{id}}", VERSION),
                new {action = "Get", controller = "Customers"},
                new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)});

            config.Routes.MapHttpRoute("RESTUpdate", string.Format("v{0}/customers/{{id}}", VERSION),
                new {action = "Update", controller = "Customers"},
                new {httpMethod = new HttpMethodConstraint(new HttpMethod("PATCH"))});

            config.Routes.MapHttpRoute("RESTDelete", string.Format("v{0}/customers/{{id}}", VERSION),
                new {action = "Delete", controller = "Customers"},
                new {httpMethod = new HttpMethodConstraint(HttpMethod.Delete)});

            config.Routes.MapHttpRoute("RESTAddOrder", string.Format("v{0}/customers/{{id}}/orders", VERSION),
                new { action = "AddOrder", controller = "Customers" },
                new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            return config;
        }

        public void Dispose()
        {
            Stop();
        }
    }
}
