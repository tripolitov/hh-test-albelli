﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Domain;

namespace Application.WebApi.Model
{
    public class OrderModel
    {
        public OrderModel()
        {
            
        }
        public OrderModel(Order order)
        {
            CreateDate = order.CreateDate;
            Price = order.Price;
        }
        public DateTime? CreateDate { get; set; }
        public Decimal? Price { get; set; }

    }
}
