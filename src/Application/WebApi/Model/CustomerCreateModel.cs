﻿namespace Application.WebApi.Model
{
    public class CustomerCreateModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public class CustomerUpdateModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}