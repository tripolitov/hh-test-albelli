﻿using System;
using System.Linq;
using Application.Domain;

namespace Application.WebApi.Model
{
    public class CustomerModel
    {
        public CustomerModel()
        {
            
        }

        public CustomerModel(Customer customer)
        {
            Id = customer.Id;
            Name = customer.Name;
            Email = customer.Email;
            Orders = customer.Orders.Select(o => new OrderModel(o)).ToArray();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public OrderModel[] Orders { get; set; }
    }
}