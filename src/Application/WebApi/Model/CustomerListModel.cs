﻿using System;
using System.Runtime.InteropServices;
using Application.Domain;

namespace Application.WebApi.Model
{
    public class CustomerListModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public CustomerListModel()
        {
        }

        public CustomerListModel(Customer customerModel)
        {
            if (customerModel == null) throw new ArgumentNullException(nameof(customerModel));

            Id = customerModel.Id;
            Name = customerModel.Name;
            Email = customerModel.Email;
        }
    }
}