﻿using System.Data.Entity.ModelConfiguration;
using Application.Domain;

namespace Application.Data.Mapping
{
    internal sealed class OrdersMapping : EntityTypeConfiguration<Order>
    {
        public OrdersMapping()
        {
            ToTable("Orders");
            HasKey(x => x.Id);
            HasRequired(x => x.Customer);
            Property(x => x.CreateDate).IsRequired();
            Property(x => x.Price).IsRequired();
        }
    }
}