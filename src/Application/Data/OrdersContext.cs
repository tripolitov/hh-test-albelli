﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Domain;

namespace Application.Data
{
    internal class OrdersContext : DbContext
    {
        public OrdersContext(string connectionString)
            : base(connectionString)
        {
            Database.SetInitializer<OrdersContext>(null);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.ValidateOnSaveEnabled = false;
        }
        public IDbSet<Customer> Customers { get; set; }
    }
}
