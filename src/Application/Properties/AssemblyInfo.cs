﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Castle.Core.Internal;

[assembly: AssemblyTitle("Albelli.Orders")]
[assembly: AssemblyDescription("Implementation of Albelli Orders API")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Albelli")]
[assembly: AssemblyProduct("Albelli.Orders")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("27de5f11-1aad-4142-a743-85a5ea903263")]
[assembly: InternalsVisibleTo(InternalsVisible.ToDynamicProxyGenAssembly2)]
[assembly: InternalsVisibleTo("Tests")]

//TODO[MT]: should be replaces by CI with build version
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

