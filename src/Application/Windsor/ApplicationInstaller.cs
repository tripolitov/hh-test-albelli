﻿using System;
using System.Data.Entity;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using Application.Data;
using Application.Host;
using Application.Services;
using Application.WebApi;
using Application.WebApi.Windsor;
using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Application.Windsor
{
    class ApplicationInstaller : IWindsorInstaller
    {
        private readonly Configuration m_Configuration;

        public ApplicationInstaller(Configuration configuration)
        {
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));
            m_Configuration = configuration;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes.FromThisAssembly()
                    .IncludeNonPublicTypes()
                    .BasedOn<ApiController>()
                    .LifestyleScoped(),
                Classes.FromThisAssembly()
                    .IncludeNonPublicTypes()
                    .BasedOn<DelegatingHandler>()
                    .LifestyleSingleton()
                    .WithServiceSelf(),
                Component.For<IHttpControllerActivator>()
                    .ImplementedBy<WindsorHttpControllerActivator>(),

                Component.For<ICustomersRepository>()
                    .ImplementedBy<CustomersRepository>(),

                Component.For<OrdersContext>()
                    .ImplementedBy<OrdersContext>()
                    .DependsOn(new { connectionString = m_Configuration.ConnectionString })
                    .LifestyleTransient(),

                Component.For<ApiHost>()
                    .DependsOn(new { baseUrl = m_Configuration.BaseUrl }));
        }
    }
}