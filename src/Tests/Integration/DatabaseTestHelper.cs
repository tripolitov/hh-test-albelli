using System;
using System.Data.SqlClient;

namespace Tests.Integration
{
    public static class DatabaseTestHelper
    {
        public static void Setup(string connectionString)
        {
            TearDown(connectionString);
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                foreach (var script in Resources.DbScripts._01_Create_Tables.Split(new string[] {"GO"}, StringSplitOptions.RemoveEmptyEntries))
                {
                    using (var command = new SqlCommand(script, connection))
                        command.ExecuteNonQuery();
                }
            }
        }

        public static void TearDown(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                foreach (var script in Resources.DbScripts._00_Drop_Tables.Split(new string[] {"GO"}, StringSplitOptions.RemoveEmptyEntries))
                {
                    using (var command = new SqlCommand(script, connection))
                        command.ExecuteNonQuery();
                }
            }
        }
    }
}