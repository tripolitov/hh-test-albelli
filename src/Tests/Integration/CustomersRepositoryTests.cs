﻿using System;
using System.Configuration;
using Application.Data;
using Application.Domain;
using Application.Services;
using NUnit.Framework;

namespace Tests.Integration
{
    [TestFixture]
    public class CustomersRepositoryTests
    {
        private readonly string m_ConnectionString = ConfigurationManager.ConnectionStrings["test"].ConnectionString;

        [OneTimeSetUp]
        public void Setup()
        {
            DatabaseTestHelper.Setup(m_ConnectionString);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            DatabaseTestHelper.TearDown(m_ConnectionString);
        }

        [Test]
        public void AddCustomerTest()
        {
            var cr = new CustomersRepository(new OrdersContext(m_ConnectionString));
            var x = cr.Add(new Customer() {Name = "a", Email = "c@a.c"});
            Assert.IsNotNull(cr.Get(x.Id));
        }

        [Test]
        public void AddCustomerAndDeleteTest()
        {
            var cr = new CustomersRepository(new OrdersContext(m_ConnectionString));
            Customer customer = null;
            customer = cr.Add(new Customer() { Name = "a", Email = "c@a.c" });

            int customerId = customer.Id;
            customer = cr.Get(customerId);
            Assert.IsNotNull(customer);

            cr.Delete(customer);
            customer = cr.Get(customerId);
            Assert.IsNull(customer);
        }

        [Test]
        public void AddCustomerAndUpdateTest()
        {
            var cr = new CustomersRepository(new OrdersContext(m_ConnectionString));
            Customer customer = null;
            customer = cr.Add(new Customer() { Name = "a", Email = "c@a.c" });
            int customerId = customer.Id;

            customer = cr.Get(customerId);
            Assert.IsNotNull(customer);
            customer.Name = "b";

            customer = cr.Update(customer);
            Assert.IsNotNull(customer);
            customer = cr.Get(customerId);
            Assert.IsNotNull(customer);
            Assert.AreEqual("b", customer.Name);
        }

        [Test]
        public void AddCustomerAndOrderTest()
        {
            var cr = new CustomersRepository(new OrdersContext(m_ConnectionString));
            Customer customer = null;
            customer = cr.Add(new Customer() { Name = "a", Email = "c@a.c" });
            int customerId = customer.Id;
            customer = cr.Get(customerId);
            Assert.IsNotNull(customer);

            customer.Orders.Add(new Order() {CreateDate = DateTime.Now.Date, Price = 100.12M});
            cr.Update(customer);

            customer = cr.Get(customerId);
            Assert.IsNotNull(customer);
            Assert.IsNotNull(customer.Orders);
            Assert.IsNotEmpty(customer.Orders);
        }
    }
}
