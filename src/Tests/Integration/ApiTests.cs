﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Application.Host;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;

namespace Tests.Integration
{
    [TestFixture]
    public class ApiTests
    {
        private IDisposable m_App = null;
        private Application.Host.Configuration m_Configuration;

        [OneTimeSetUp]
        public void Setup()
        {
            m_Configuration = Application.Host.Configuration.FromAppConfig();
            DatabaseTestHelper.Setup(m_Configuration.ConnectionString);
            m_App = CompositionRoot.Compose(m_Configuration);
        }

        [Test]
        [TestCase("/v1/customers", ExpectedResult = HttpStatusCode.OK)]
        [TestCase("/v1/customers/404", ExpectedResult = HttpStatusCode.NotFound)]
        [TestCase("/v1/customers/0", ExpectedResult = HttpStatusCode.BadRequest)]
        [TestCase("/v1/customers/-1", ExpectedResult = HttpStatusCode.BadRequest)]
        public HttpStatusCode CustomersGetRoutesTests(string resource)
        {
            var client = new RestClient(m_Configuration.BaseUrl);

            var request = new RestRequest(resource, Method.GET);

            var response = client.Execute(request);
            
            return response.StatusCode;
        }

        [Test]
        public void CreateCustomerTest()
        {
            var customerToCreate = new { name = "John", email = "john@mail.test" };

            var client = new RestClient(m_Configuration.BaseUrl);

            var request = new RestRequest("v1/customers", Method.POST);
            request.AddJsonBody(customerToCreate);
            request.AddHeader("Accept", "application/json");
            var response = client.Execute(request);

            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);

            dynamic createdCustomer = JObject.Parse(response.Content);

            Assert.AreEqual(customerToCreate.name, (string)createdCustomer.Name);
            Assert.AreEqual(customerToCreate.email, (string)createdCustomer.Email);
            Assert.Greater(((int)createdCustomer.Id), 0);
        }


        [Test]
        public void CreatedCustomerCanBeLoadedByIdTest()
        {
            var customerToCreate = new { name = "John", email = "john@mail.test" };

            var client = new RestClient(m_Configuration.BaseUrl);
            var request = new RestRequest("v1/customers", Method.POST);
            request.AddJsonBody(customerToCreate);
            request.AddHeader("Accept", "application/json");
            var responseFromCreate = client.Execute(request);

            Assert.AreEqual(HttpStatusCode.Created, responseFromCreate.StatusCode);

            dynamic createdCustomer = JObject.Parse(responseFromCreate.Content);

            var createdCustomerId = ((int)createdCustomer.Id);
            Assert.Greater(createdCustomerId, 0);

            var responseFromGet = client.Execute(new RestRequest(string.Format("v1/customers/{0}", createdCustomerId), Method.GET));
            Assert.AreEqual(HttpStatusCode.OK, responseFromGet.StatusCode);

            dynamic gotCustomer = JObject.Parse(responseFromGet.Content);
            Assert.AreEqual(createdCustomerId, ((int)gotCustomer.Id));
            Assert.AreEqual(customerToCreate.name, (string)gotCustomer.Name);
            Assert.AreEqual(customerToCreate.email, (string)gotCustomer.Email);
        }

        [Test]
        public void CreatedCustomerCanBeLoadedTest()
        {
            var customerToCreate = new { name = "John", email = "john@mail.test" };

            var client = new RestClient(m_Configuration.BaseUrl);
            var request = new RestRequest("v1/customers", Method.POST);
            request.AddJsonBody(customerToCreate);
            request.AddHeader("Accept", "application/json");
            var responseFromCreate = client.Execute(request);

            Assert.AreEqual(HttpStatusCode.Created, responseFromCreate.StatusCode);

            dynamic createdCustomer = JObject.Parse(responseFromCreate.Content);

            var createdCustomerId = ((int)createdCustomer.Id);
            Assert.Greater(createdCustomerId, 0);

            var responseFromGet = client.Execute(new RestRequest("v1/customers", Method.GET));
            Assert.AreEqual(HttpStatusCode.OK, responseFromGet.StatusCode);

            var customers = JArray.Parse(responseFromGet.Content);
            Assert.Greater(((int)customers.Count), 0);
            Assert.IsTrue(customers.Select((dynamic customer) => (int) customer.Id).Any(id => id == createdCustomerId));
        }

        [Test]
        public void CreatedCustomerCanBeUpdatedByIdTest()
        {
            var customerToCreate = new { name = "John", email = "john@mail.test" };
            var customerToUpdate = new { name = "John Doe", email = "john@email.test" };

            var client = new RestClient(m_Configuration.BaseUrl);
            var createRequest = new RestRequest("v1/customers", Method.POST);
            createRequest.AddJsonBody(customerToCreate);
            createRequest.AddHeader("Accept", "application/json");
            var responseFromCreate = client.Execute(createRequest);

            Assert.AreEqual(HttpStatusCode.Created, responseFromCreate.StatusCode);

            dynamic createdCustomer = JObject.Parse(responseFromCreate.Content);

            var createdCustomerId = ((int)createdCustomer.Id);
            Assert.Greater(createdCustomerId, 0);

            var responseFromGet = client.Execute(new RestRequest(string.Format("v1/customers/{0}", createdCustomerId), Method.GET));
            Assert.AreEqual(HttpStatusCode.OK, responseFromGet.StatusCode);

            dynamic gotCustomer = JObject.Parse(responseFromGet.Content);
            Assert.AreEqual(createdCustomerId, ((int)gotCustomer.Id));
            Assert.AreEqual(customerToCreate.name, (string)gotCustomer.Name);
            Assert.AreEqual(customerToCreate.email, (string)gotCustomer.Email);

            var updateRequest = new RestRequest(string.Format("v1/customers/{0}", createdCustomerId), Method.PATCH);
            updateRequest.AddJsonBody(customerToUpdate);
            updateRequest.AddHeader("Accept", "application/json");
            var responseFromUpdate = client.Execute(updateRequest);
            Assert.AreEqual(HttpStatusCode.OK, responseFromUpdate.StatusCode);

            dynamic updatedCustomer = JObject.Parse(responseFromUpdate.Content);

            Assert.AreEqual(createdCustomerId, ((int)updatedCustomer.Id));
            Assert.AreEqual(customerToUpdate.name, (string)updatedCustomer.Name);
            Assert.AreEqual(customerToUpdate.email, (string)updatedCustomer.Email);
        }

        [Test]
        public void CreatedCustomerCanBeDeletedByIdTest()
        {
            var customerToCreate = new { name = "John", email = "john@mail.test" };

            var client = new RestClient(m_Configuration.BaseUrl);
            var request = new RestRequest("v1/customers", Method.POST);
            request.AddJsonBody(customerToCreate);
            request.AddHeader("Accept", "application/json");
            var responseFromCreate = client.Execute(request);

            Assert.AreEqual(HttpStatusCode.Created, responseFromCreate.StatusCode);

            dynamic createdCustomer = JObject.Parse(responseFromCreate.Content);

            var createdCustomerId = ((int)createdCustomer.Id);
            Assert.Greater(createdCustomerId, 0);

            var responseFromDelete = client.Execute(new RestRequest(string.Format("v1/customers/{0}", createdCustomerId), Method.DELETE));
            Assert.AreEqual(HttpStatusCode.OK, responseFromDelete.StatusCode);

            var responseFromGet = client.Execute(new RestRequest(string.Format("v1/customers/{0}", createdCustomerId), Method.GET));
            Assert.AreEqual(HttpStatusCode.NotFound, responseFromGet.StatusCode);
        }

        [Test]
        public void CreateCustomerAndAddOrderTest()
        {
            var customerToCreate = new { name = "John", email = "john@mail.test" };
            var orderToCreate = new { createDate = new DateTime(2015, 12, 31), price = 100 };

            var client = new RestClient(m_Configuration.BaseUrl);

            var createRequest = new RestRequest("v1/customers", Method.POST);
            createRequest.AddJsonBody(customerToCreate);
            createRequest.AddHeader("Accept", "application/json");
            var responseFromCreate = client.Execute(createRequest);
            Assert.AreEqual(HttpStatusCode.Created, responseFromCreate.StatusCode);
            dynamic createdCustomer = JObject.Parse(responseFromCreate.Content);
            var createdCustomerId = ((int)createdCustomer.Id);

            var addOrderRequest = new RestRequest(string.Format("v1/customers/{0}/orders", createdCustomerId), Method.POST);
            addOrderRequest.AddJsonBody(orderToCreate);
            addOrderRequest.AddHeader("Accept", "application/json");

            var responseFromAddOrder = client.Execute(addOrderRequest);
            Assert.AreEqual(HttpStatusCode.Created, responseFromAddOrder.StatusCode);

            var responseFromGet = client.Execute(new RestRequest(string.Format("v1/customers/{0}", createdCustomerId), Method.GET));
            Assert.AreEqual(HttpStatusCode.OK, responseFromGet.StatusCode);

            dynamic gotCustomer = JObject.Parse(responseFromGet.Content);
            Assert.AreEqual(createdCustomerId, ((int)gotCustomer.Id));
            Assert.AreEqual(customerToCreate.name, (string)gotCustomer.Name);
            Assert.AreEqual(customerToCreate.email, (string)gotCustomer.Email);
            Assert.IsNotNull((object)gotCustomer.Orders);
            Assert.AreEqual(1, (int)(gotCustomer.Orders.Count));

        }

        [OneTimeTearDown]
        public void TearDown()
        {
            if(m_App != null)
                m_App.Dispose();

            DatabaseTestHelper.TearDown(m_Configuration.ConnectionString);
        }
    }
}
