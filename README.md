# README #

## Design decisions ##
I decided to implement test as console application with self hosted OWIN based web api so it can be started as console by executing exe file or as a service. StartableFacility is utilized in composition root this allows just to register IStartable components to container to make them start.

Following routes are acailiable:

* GET /v1/customers to get list of customers
* POST /v1/customers to create new customer
* GET /v1/customers/{id} to get customer by id
* PATCH /v1/customers/{id} to update customer name or email or both
* DELETE /v1/customers/{id} to delete existing cutomer
* POST /v1/customers/{id}/orders to create order for existing cutomer

WebApi provides both json ans xml serialization via content negotiation based on Accept http header.

I decided to implement only integration tests this allows me to test everything throw api via RestSharp client. This gives enough coverage.

## TODO ##

* utilize FluentValidator to validate input data (e.g. check that email is in correct format and price of order is not negative).
* refactor unit test to make them less verbose, use given-when-then, increase coverage 
* introduce api documentation based on xml docs

## Configuration ##
Conectionе string is configured via <connectionStrings> section of app.config file - application requires connection string with name "main" to be configured.
Host and port to listen to can be configured via <appSettings> key with names host and port.

## Deployment ##
###Database##
Under src/db folder scripts to recreate database can be found. 

###Service###
To register application as windows service command can be used
```
#!c#

sc.exe create "Albelli Orders API" obj= "[.\username]" password= "[password]" binPath= "d:\albelli-orders\application.exe" start= auto 
```
if needed for [.\username] rights url acl should be extended with following command

```
#!c#

netsh http add urlacl url=http://localhost:8080/ user=[.\username]
```